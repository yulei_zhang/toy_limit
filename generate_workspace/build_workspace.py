import ROOT

import generate_hist as gh

RooStats = ROOT.RooStats
HistFactory = RooStats.HistFactory


def make_workspace():
    measure = HistFactory.Measurement('llp', 'llp')
    measure.SetOutputFilePrefix("temp")
    measure.AddPOI('mu_nDLLP_1')
    # measure.AddPOI('mu_nDLLP_2')

    # overall luminosity uncertainties
    measure.SetLumi(1.0)
    measure.AddConstantParam("Lumi")
    measure.SetLumiRelErr(0.0001)

    measure.SetExportOnly(True)
    # measure.SetBinHigh(10)

    measure.AddChannel(add_channel("SR1"))
    # measure.AddChannel(add_channel("SR2"))

    measure.CollectHistograms()
    measure.PrintTree()
    measure.PrintXML(f"xml_llp", measure.GetOutputFilePrefix())
    w = HistFactory.MakeModelAndMeasurementFast(measure)
    w.SetName("combined")
    w.Print("t")
    w.writeToFile("125.root")


def add_channel(channel_name: str):
    channel_file = f"{channel_name}.root"
    # create a channel
    chan = HistFactory.Channel(channel_name)
    chan.SetData("data", channel_file)
    chan.SetStatErrorConfig(0.0, "Poisson")

    nDLLP_1 = HistFactory.Sample("nDLLP_1", "nDLLP_1", channel_file)
    # nDLLP_1.AddOverallSys("syst1", 0.95, 1.05)
    nDLLP_1.AddNormFactor("mu_nDLLP_1", 1.0, 0, 40)
    chan.AddSample(nDLLP_1)

    # nDLLP_2 = HistFactory.Sample("nDLLP_2", "nDLLP_2", channel_file)
    # nDLLP_2.AddNormFactor("mu_nDLLP_2", 1, 0, 2)
    # chan.AddSample(nDLLP_2)

    nDLLP_0 = HistFactory.Sample("nDLLP_0", "nDLLP_0", channel_file)
    nDLLP_0.ActivateStatError()
    chan.AddSample(nDLLP_0)

    return chan


if __name__ == "__main__":
    make_workspace()

    pass
