import ROOT
import os

import numpy as np

nbins = 1
xmin = 0
xmax = 1


class HistBase:
    def __init__(self):
        self.hists = {
            "nDLLP_0": None,
            "nDLLP_1": None,
            "nDLLP_2": None,
        }


class Region:
    def __init__(self, name: str = "SR"):
        self.name = name
        self.data = ROOT.TH1F(f"data_{name}", f"data_{name}", nbins, xmin, xmax)
        self.nominal = []

    def make_data(self, hb: HistBase):
        for _, v in hb.hists.items():
            self.data.Add(v)

        self.nominal = list(hb.hists.values())

    def save_to_disk(self, out_dir):
        if not os.path.isdir(out_dir):
            os.makedirs(out_dir)

        f = ROOT.TFile(f'{self.name}.root', "RECREATE")
        f.cd()

        for hist in [self.data, *self.nominal]:
            hist_name = hist.GetName().removesuffix(f'_{self.name}')
            hist.Write(hist_name)
            print(f'==> Write {hist_name}')
        f.Close()

        print(f'==> Save to {os.path.join(out_dir, f"{self.name}")}')


def define_sr_hists(name: str) -> HistBase:
    hb = HistBase()

    for i, h in enumerate(hb.hists):
        hb.hists[h] = ROOT.TH1F(f"{h}_{name}", f"{h}_{name}", nbins, xmin, xmax)
        hb.hists[h].Sumw2()

    return hb


def fill_hists(hb: HistBase):
    hb.hists["nDLLP_0"].SetBinContent(1, 1.0)
    hb.hists["nDLLP_1"].SetBinContent(1, 5e5)
    # hb.hists["nDLLP_2"].SetBinContent(1, 1e4)

    hb.hists["nDLLP_0"].SetBinError(1, 0.01)
    hb.hists["nDLLP_1"].SetBinError(1, 1./np.sqrt(5e5))
    # hb.hists["nDLLP_2"].SetBinError(1, 0.5)


if __name__ == "__main__":
    r1 = define_sr_hists("SR1")
    r2 = define_sr_hists("SR2")

    fill_hists(r1)
    fill_hists(r2)

    r = Region("SR1")
    r.make_data(r1)
    r.save_to_disk(".")
    r = Region("SR2")
    r.make_data(r2)
    r.save_to_disk(".")
