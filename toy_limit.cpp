//
// Created by Zhang Yulei on 1/20/23.
//

#include "argparse.h"
#include "HypoTestInv.h"

#include "TGraph.h"
#include "TMultiGraph.h"
#include "TFile.h"
#include "string"


#include "RooWorkspace.h"
#include "RooStats/HypoTestInverterPlot.h"
#include "RooStats/HypoTestInverterResult.h"

#include <fstream>
#include <filesystem>

namespace fs = std::filesystem;

int main(int argc, char *argv[]) {
    argparse::ArgumentParser program("ToyLimit", "1.0.0");

    program.add_argument("poi")
            .help("Parameter of interest for this scan");

    program.add_argument("--value")
            .help("fixed value of poi")
            .default_value(1.0)
            .scan<'g', float>();

    program.add_argument("--itr")
            .help("iteration of this poi (for record only)")
            .default_value(1)
            .scan<'i', int>();

    program.add_argument("--out_dir")
            .help("output path for this result")
            .default_value(std::string("."));

    program.add_argument("--in_file")
            .help("input path for workspace")
            .required();

    program.add_argument("--prerun")
            .help("pre-scan to get the fined scan interval")
            .default_value(false)
            .implicit_value(true);

    try {
        program.parse_args(argc, argv);
    }
    catch (const std::runtime_error &err) {
        std::cerr << err.what() << std::endl;
        std::cerr << program;
        std::exit(1);
    }

    auto prerun = program.get<bool>("--prerun");
    auto POI = program.get<std::string>("poi");
    auto POI_value = program.get<float>("value");
    auto POI_itr = program.get<int>("itr");
    const auto &out_dir = program.get<std::string>("out_dir");
    const auto &in_file = program.get<std::string>("in_file");
    std::cout << " ==> In file: " << in_file << std::endl;
    std::cout << " ==> Output dir: " << out_dir << std::endl;
    std::cout << "itr: " << POI_itr << " -- " << POI << ": " << POI_value << std::endl;


//    auto POI = "SigXsecOverSM_nDLLP1";
//    auto POI_value = 1e-4;
    int ntoys = 5e3;
    float scan_points = 50;

    auto *file = TFile::Open(in_file.data());
    auto *w = dynamic_cast<RooWorkspace *>( file->Get("combined"));
    RooStats::HypoTestInverterResult *r;

    if (w != nullptr) {
        HypoTestInv hti(w);

        double v_min = 1.0;
        double v_max = 1.0;

        fs::path prerun_path = out_dir;
        prerun_path /= TString::Format("%s_prerun.txt", POI.data()).Data();

        if (prerun) {
            v_min = 5e-7;
            v_max = 1.0;

            // first fitting for found interval
            r = hti.RunPoint(POI, ntoys, 10, v_min, v_max);

            auto p = RooStats::HypoTestInverterPlot("HTI_Result_Plot", "", r).MakeExpectedPlot();
            auto exp_plot = dynamic_cast<TGraph * >(p->GetListOfGraphs()->At(2));
            for (int i = 1; i < exp_plot->GetN(); ++i) {
                if (auto cur_y = *(exp_plot->GetY() + i); cur_y <= 0.05) {
                    v_min = *(exp_plot->GetX() + i - 1) * 0.5;
                    v_max = *(exp_plot->GetX() + i) * 2;

                    std::cout << "95% point found. [" << v_min << ", " << v_max << "]" << std::endl;

                    if (!fs::exists(out_dir)) {
                        fs::create_directory(out_dir);
                    }
                    // record the prerun results
                    std::ofstream prerun_result(prerun_path);
                    prerun_result << POI << " " << v_min << " " << v_max << std::endl;
                    prerun_result.close();

                    std::cout << "[Prerun] ==> Result saved to " << prerun_path << std::endl;

                    return 0;
                }
            }
        }

        // read prerun result
        std::string prerun_poi;
        std::ifstream prerun_in(prerun_path);
        if (!prerun_in.good()) {
            std::cerr << "Not prerun scan file:" << prerun_path << " - Exit " << std::endl;
            return -1;
        }
        prerun_in >> prerun_poi >> v_min >> v_max;
        std::cout << prerun_poi << " " << v_min << " " << v_max << std::endl;

        double v = v_min + (v_max - v_min) / scan_points * POI_itr;

        r = hti.RunPoint(POI, ntoys, 1, v, v);

        if (!r) {
            std::cerr << "Error running the HypoTestInverter - Exit " << std::endl;
            return -1;
        }

//        auto out_dir = "";

        hti.RecordResult(r, POI_itr, out_dir);
    } else
        return -1;
}