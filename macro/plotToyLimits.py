import ROOT as r

# prefix = r"/Users/avencast/CLionProjects/wsmakerllp/workspace/output/llp.only1_fullRes_llp_only1/limitToys/"
# print(os.listdir(prefix))

# for filename in os.listdir(prefix):
#     if filename.endswith(".root"):
#         ftmp = r.TFile(os.path.join(prefix,filename), "read")
#         rtmp = ftmp.Get("result_SigXsecOverSM_nDLLP1")
#
#         if idx == 0:
#             res = rtmp.Clone()
#             print('hey')
#         else:
#             res.Add(rtmp)
#
#         idx+=1

f = r.TFile('mu_nDLLP_1_1.root', "read")
res = f.Get("result_mu_nDLLP_1")

plot = r.RooStats.HypoTestInverterPlot("HTI_Result_Plot", "", res)
c = r.TCanvas()
plot.Draw('EXP')
# c.SaveAs("toylimit.svg")

print("Expected upper limits, using the B (alternate) model : ")
print(" expected limit (median) ", res.GetExpectedUpperLimit(0))
print(" expected limit (-1 sig) ", res.GetExpectedUpperLimit(-1))
print(" expected limit (+1 sig) ", res.GetExpectedUpperLimit(1))
print(" expected limit (-2 sig) ", res.GetExpectedUpperLimit(-2))
print(" expected limit (+2 sig) ", res.GetExpectedUpperLimit(2))

