//
// Created by Zhang Yulei on 1/20/23.
//

#include "HypoTestInv.h"
#include "TError.h"

#include <filesystem>//for filesystem
#include <vector>

namespace fs = std::filesystem;
using vector_str = std::vector<TString>;

HypoTestInv::HypoTestInv() = default;

HypoTestInv::~HypoTestInv() {
    delete w;
    delete r;
}

HTIResult *HypoTestInv::RunPoint(const TString &POI, int ntoys, int v_bin, double v_min, double v_max) {
    std::cout << "[Scan] ==> " << POI << ", " << ntoys << ", " << v_bin << ", " << v_min << ", " << v_max << std::endl;
    std::cout << "Running HypoTestInverter on the workspace " << w->GetName() << std::endl;

    w->Print();

    RooAbsData *data = w->data(dataName);
    if (!data) {
        Error("HypoTestInv", "Not existing data %s", dataName.Data());
        return nullptr;
    } else
        std::cout << "Using data set " << dataName << std::endl;

    if (optHTInv.useVectorStore) {
        RooAbsData::setDefaultStorageType(RooAbsData::Vector);
        data->convertToVectorStore();
    }

    // get models from WS
    // get the modelConfig out of the file
    MC *bModel = dynamic_cast<MC *>(w->obj(modelBName));
    MC *sbModel = dynamic_cast<MC *>(w->obj(modelSBName));

    if (!sbModel) {
        Error("StandardHypoTestDemo", "Not existing ModelConfig %s", modelSBName.Data());
        return nullptr;
    }
    if (!sbModel->GetPdf()) {
        Error("HypoTestInv", "Model %s has no pdf ", modelSBName.Data());
        return nullptr;
    }
    if (!sbModel->GetParametersOfInterest()) {
        Error("HypoTestInv", "Model %s has no poi ", modelSBName.Data());
        return nullptr;
    }
    if (!sbModel->GetObservables()) {
        Error("HypoTestInv", "Model %s has no observables ", modelSBName.Data());
        return nullptr;
    }
    if (!sbModel->GetSnapshot()) {
        Info("HypoTestInv", "Model %s has no snapshot  - make one using model poi", modelSBName.Data());
        sbModel->SetSnapshot(*sbModel->GetParametersOfInterest());
    }

    // case of no systematics
    // remove nuisance parameters from model
    if (optHTInv.noSystematics) {
        const RooArgSet *nuisPar = sbModel->GetNuisanceParameters();
        if (nuisPar && nuisPar->getSize() > 0) {
            std::cout << "HypoTestInv"
                      << "  -  Switch off all systematics by setting them constant to their initial values"
                      << std::endl;
            RooStats::SetAllConstant(*nuisPar);
        }
        if (bModel) {
            const RooArgSet *bnuisPar = bModel->GetNuisanceParameters();
            if (bnuisPar)
                RooStats::SetAllConstant(*bnuisPar);
        }
    }

    vector_str poi_list{};
    TString global_obs_str = "";
    // Select only one POI, and move others to nuisance parameters
    for (auto poi: *sbModel->GetParametersOfInterest()) {
        if (poi->GetName() != POI)
            global_obs_str += poi->GetName() + TString(",");
        poi_list.emplace_back(poi->GetName());
    }
    global_obs_str.Remove(TString::EStripType::kTrailing, ',');
    sbModel->SetParametersOfInterest(POI);
//    sbModel->SetGlobalObservables(global_obs_str);
//    sbModel->SetNuisanceParameters(global_obs_str);
    currentPOI = POI;

    if (!bModel || bModel == sbModel) {
        Info("HypoTestInv", "The background model %s does not exist", modelBName.Data());
        Info("HypoTestInv", "Copy it from ModelConfig %s and set POI to zero", modelSBName.Data());
        bModel = dynamic_cast<MC *>(sbModel->Clone());
        bModel->SetName(TString(modelSBName) + TString("_with_poi_0"));
        auto *var = dynamic_cast<RooRealVar *>(bModel->GetParametersOfInterest()->first());
        if (!var) return nullptr;
        double oldval = var->getVal();
        var->setVal(0);
        bModel->SetSnapshot(RooArgSet(*var));
        var->setVal(oldval);
    } else {
        if (!bModel->GetSnapshot()) {
            Info(
                    "HypoTestInv",
                    "Model %s has no snapshot  - make one using model poi and 0 values ",
                    modelBName.Data()
            );
            bModel->SetParametersOfInterest(POI);
            auto *var = dynamic_cast<RooRealVar *>(bModel->GetParametersOfInterest()->first());
            if (var) {
                double oldval = var->getVal();
                var->setVal(0);
                bModel->SetSnapshot(RooArgSet(*var));
                var->setVal(oldval);
            } else {
                Error("HypoTestInv", "Model %s has no valid poi", modelBName.Data());
                return nullptr;
            }
        }
    }

    // check model  has global observables when there are nuisance pdf
    // for the hybrid case the globals are not needed
    bool hasNuisParam = (sbModel->GetNuisanceParameters() && sbModel->GetNuisanceParameters()->getSize() > 0);
    bool hasGlobalObs = (sbModel->GetGlobalObservables() && sbModel->GetGlobalObservables()->getSize() > 0);
    if (hasNuisParam && !hasGlobalObs) {
        // try to see if model has nuisance parameters first
        RooAbsPdf *constrPdf = RooStats::MakeNuisancePdf(*sbModel, "nuisanceConstraintPdf_sbmodel");
        if (constrPdf) {
            Warning("HypoTestInv",
                    "Model %s has nuisance parameters but no global observables associated", sbModel->GetName());
            Warning("HypoTestInv",
                    "\tThe effect of the nuisance parameters will not be treated correctly ");
        }
    }

    // save all initial parameters of the model including the global observables
    RooArgSet initialParameters;
    RooArgSet *allParams = sbModel->GetPdf()->getParameters(*data);
    allParams->snapshot(initialParameters);
    delete allParams;

    // run first a data fit
    const RooArgSet *poiSet = sbModel->GetParametersOfInterest();
    auto *poi = dynamic_cast<RooRealVar *>(poiSet->first());

    std::cout << "==> POI initial value:   " << poi->GetName() << " = " << poi->getVal() << std::endl;

    // fit the data first (need to use constraint )
    TStopwatch tw;

    bool doFit = optHTInv.initialFit;
    double poihat = 0;

    if (optHTInv.minimizerType.Length() == 0)
        optHTInv.minimizerType = ROOT::Math::MinimizerOptions::DefaultMinimizerType();
    else
        ROOT::Math::MinimizerOptions::SetDefaultMinimizer(optHTInv.minimizerType.Data());

    Info("HypoTestInv", "Using %s as minimizer for computing the test statistic",
         ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str());

    if (doFit) {

        // do the fit : By doing a fit the POI snapshot (for S+B)  is set to the fit value
        // and the nuisance parameters nominal values will be set to the fit value.
        // This is relevant when using LEP test statistics

        Info("HypoTestInv", " Doing a first fit to the observed data ");
        RooArgSet constrainParams;
        if (sbModel->GetNuisanceParameters()) constrainParams.add(*sbModel->GetNuisanceParameters());
        RooStats::RemoveConstantParameters(&constrainParams);
        tw.Start();
        RooFitResult *fitres = sbModel->GetPdf()->fitTo(
                *data,
                RooFit::InitialHesse(false),
                RooFit::Hesse(false),
                RooFit::Minimizer(optHTInv.minimizerType.Data(), "Migrad"),
                RooFit::Strategy(0),
                RooFit::PrintLevel(optHTInv.printLevel),
                RooFit::Constrain(constrainParams),
                RooFit::Save(true),
                RooFit::Offset(RooStats::IsNLLOffset()));
        if (fitres->status() != 0) {
            Warning("HypoTestInv[doFit]",
                    "Fit to the model failed - try with strategy 1 and perform first an Hesse computation");
            fitres = sbModel->GetPdf()->fitTo(
                    *data,
                    RooFit::InitialHesse(true),
                    RooFit::Hesse(false),
                    RooFit::Minimizer(optHTInv.minimizerType.Data(), "Migrad"),
                    RooFit::Strategy(1),
                    RooFit::PrintLevel(optHTInv.printLevel),
                    RooFit::Constrain(constrainParams),
                    RooFit::Save(true),
                    RooFit::Offset(RooStats::IsNLLOffset()));
        }
        if (fitres->status() != 0)
            Warning("HypoTestInv[doFit]", " Fit still failed - continue anyway.....");


        poihat = poi->getVal();
        std::cout << "HypoTestInv[doFit] - Best Fit value : " << poi->GetName() << " = "
                  << poihat << " +/- " << poi->getError() << std::endl;
        std::cout << "Time for fitting : ";
        tw.Print();

        //save best fit value in the poi snapshot
        sbModel->SetSnapshot(*sbModel->GetParametersOfInterest());
        std::cout << "HypoTestInv[doFit]: snapshot of S+B Model " << sbModel->GetName()
                  << " is set to the best fit value" << std::endl;

    }

    // build test statistics and hypotest calculators for running the inverter
    RooStats::SimpleLikelihoodRatioTestStat slrts(*sbModel->GetPdf(), *bModel->GetPdf());

    // null parameters must includes snapshot of poi plus the nuisance values
    RooArgSet nullParams(*sbModel->GetSnapshot());
    if (sbModel->GetNuisanceParameters()) nullParams.add(*sbModel->GetNuisanceParameters());
    if (sbModel->GetSnapshot()) slrts.SetNullParameters(nullParams);
    RooArgSet altParams(*bModel->GetSnapshot());
    if (bModel->GetNuisanceParameters()) altParams.add(*bModel->GetNuisanceParameters());
    if (bModel->GetSnapshot()) slrts.SetAltParameters(altParams);
    if (optHTInv.enableDetailedOutput) slrts.EnableDetailedOutput();

    // ratio of profile likelihood - need to pass snapshot for the alt
    RooStats::RatioOfProfiledLikelihoodsTestStat ropl(*sbModel->GetPdf(), *bModel->GetPdf(), bModel->GetSnapshot());
    ropl.SetSubtractMLE(false);
    ropl.SetPrintLevel(optHTInv.printLevel);
    ropl.SetMinimizer(optHTInv.minimizerType.Data());
    if (optHTInv.enableDetailedOutput) ropl.EnableDetailedOutput();

    RooStats::ProfileLikelihoodTestStat profll(*sbModel->GetPdf());
    profll.SetOneSided(true);
    profll.SetMinimizer(optHTInv.minimizerType.Data());
    profll.SetPrintLevel(optHTInv.printLevel);
    if (optHTInv.enableDetailedOutput) profll.EnableDetailedOutput();

    profll.SetReuseNLL(optHTInv.optimize);
    slrts.SetReuseNLL(optHTInv.optimize);
    ropl.SetReuseNLL(optHTInv.optimize);

    if (optHTInv.optimize) {
        profll.SetStrategy(0);
        ropl.SetStrategy(0);
        ROOT::Math::MinimizerOptions::SetDefaultStrategy(0);
    }

    if (optHTInv.maxPOI > 0) poi->setMax(optHTInv.maxPOI);  // increase limit

    RooStats::MaxLikelihoodEstimateTestStat maxll(*sbModel->GetPdf(), *poi);
    RooStats::NumEventsTestStat nevtts;

    // create the HypoTest calculator class
    RooStats::HypoTestCalculatorGeneric *hc = new RooStats::FrequentistCalculator(*data, *bModel, *sbModel);

    // set the test statistic
    RooStats::TestStatistic *testStat = &profll;

    auto *toymcs = dynamic_cast<RooStats::ToyMCSampler *>(hc->GetTestStatSampler());

    if (toymcs) {
        // look if pdf is number counting or extended
        if (sbModel->GetPdf()->canBeExtended()) {
            if (optHTInv.useNumberCounting)
                Warning("HypoTestInv[toymc]", "Pdf is extended: but number counting flag is set: ignore it ");
        } else {
            // for not extended pdf
            if (!optHTInv.useNumberCounting) {
                int nEvents = data->numEntries();
                Info("HypoTestInv[toymc]",
                     "Pdf is not extended: number of events to generate taken  from observed data set is %d", nEvents);
                toymcs->SetNEventsPerToy(nEvents);
            } else {
                Info("HypoTestInv[toymc]", "using a number counting pdf");
                toymcs->SetNEventsPerToy(1);
            }
        }
        toymcs->SetTestStatistic(testStat);

        if (data->isWeighted() && !optHTInv.generateBinned) {
            Info("HypoTestInv[toymc]",
                 "Data set is weighted, nentries = %d and sum of weights = %8.1f but toy generation is unbinned - it would be faster to set mGenerateBinned to true\n",
                 data->numEntries(), data->sumEntries());
        }
        toymcs->SetGenerateBinned(optHTInv.generateBinned);
        toymcs->SetUseMultiGen(optHTInv.optimize);

        if (optHTInv.generateBinned && sbModel->GetObservables()->getSize() > 2) {
            Warning("StandardHypoTestInvDemo",
                    "generate binned is activated but the number of observable is %d. Too much memory could be needed for allocating all the bins",
                    sbModel->GetObservables()->getSize());
        }

        // set the random seed if needed
        if (optHTInv.randomSeed >= 0) RooRandom::randomGenerator()->SetSeed(optHTInv.randomSeed);
    }

    // specify if you need to re-use same toys
    if (optHTInv.reuseAltToys) {
        hc->UseSameAltToys();
    }


    ((RooStats::FrequentistCalculator *) hc)->SetToys(ntoys, int(ntoys / optHTInv.nToysRatio));
    // store also the fit information for each poi point used by calculator based on toys
    if (optHTInv.enableDetailedOutput) ((RooStats::FrequentistCalculator *) hc)->StoreFitInfo(true);


    // Get the result
    RooMsgService::instance().getStream(1).removeTopic(RooFit::NumIntegration);


    RooStats::HypoTestInverter calc(*hc);
    calc.SetConfidenceLevel(optHTInv.confLevel);

    calc.UseCLs(useCLs);
    calc.SetVerbose(true);

    // can speed up using proof-lite
    if (optHTInv.useProof) {
        RooStats::ProofConfig pc(*w, optHTInv.nworkers, "workers=10", kFALSE);
        toymcs->SetProofConfig(&pc);    // enable proof
    }

    std::cout << "Scan from " << v_min << " to " << v_max << " with bin: " << v_bin << std::endl;
    calc.SetFixedScan(v_bin, v_min, v_max, true);

    std::cout << "Start to GetInterval" << std::endl;
    tw.Start();
    HTIResult *result = calc.GetInterval();
    std::cout << "Time to perform limit scan \n";
    tw.Print();

    return result;
}

void HypoTestInv::RecordResult(HTIResult *result, int itr, const TString &out_dir) {
    Info("Record Results", "detailed output will be written in output result file");

    fs::path out_path = out_dir.Data();
    // write result in a file
    if (result != nullptr) {
        if (!fs::exists(out_path)) {
            fs::create_directory(out_path);
        }
        // write to a file the results
        TString mResultFileName = TString::Format("%s_%d.root", currentPOI.Data(), itr);

        out_path /= mResultFileName.Data();

        auto *fileOut = new TFile(out_path.c_str(), "RECREATE");
        result->Write();
        Info(
                "RecordResult",
                "HypoTestInverterResult has been written in the file %s",
                out_path.c_str()
        );

        fileOut->Close();
    }


}
