//
// Created by Zhang Yulei on 1/20/23.
//

#ifndef TOYLIMIT_HYPOTESTINV_H
#define TOYLIMIT_HYPOTESTINV_H

/// Freq calculator
/// testStatType = Profile Likelihood one sided (i.e. = 0 if mu < mu_hat)

#include "TFile.h"
#include "RooWorkspace.h"
#include "RooAbsPdf.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooStats/ModelConfig.h"
#include "RooRandom.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TROOT.h"
#include "TSystem.h"

#include "RooStats/FrequentistCalculator.h"
#include "RooStats/ToyMCSampler.h"
#include "RooStats/HypoTestPlot.h"

#include "RooStats/NumEventsTestStat.h"
#include "RooStats/ProfileLikelihoodTestStat.h"
#include "RooStats/SimpleLikelihoodRatioTestStat.h"
#include "RooStats/RatioOfProfiledLikelihoodsTestStat.h"
#include "RooStats/MaxLikelihoodEstimateTestStat.h"
#include "RooStats/NumEventsTestStat.h"

#include "RooStats/HypoTestInverter.h"
#include "RooStats/HypoTestInverterResult.h"
#include "RooStats/HypoTestInverterPlot.h"

#include <cassert>
#include <utility>

using HTIResult = RooStats::HypoTestInverterResult;
using MC = RooStats::ModelConfig;

// structure defining the options
struct HypoTestInvOptions {

    TString resultFileName;                  // file with results (by default is built automatically using the workspace input file name)
    bool optimize = true;                    // optimize evaluation of test statistic
    bool useVectorStore = true;              // convert data to use new roofit data store
    bool generateBinned = true;             // generate binned data sets
    bool noSystematics = false;              // force all systematics to be off (i.e. set all nuisance parameters as constant to their nominal values)
    double nToysRatio = 2;                   // ratio Ntoys S+b/ntoysB
    double maxPOI = -1;                      // max value used of POI (in case of auto scan)
    bool useProof = false;                   // use Proof Lite when using toys (for freq or hybrid)
    int nworkers = 0;                        // number of worker for ProofLite (default use all available cores)
    bool enableDetailedOutput = true;        // enable detailed output with all fit information for each toy (output will be written in result file)

    int initialFit = -1;                     // do a first  fit to the model (-1 : default, 0 skip fit, 1 do always fit)
    int randomSeed = 0;                      // random seed (if = -1: use default value, if = 0 always random )

    bool reuseAltToys = true;               // reuse same toys for alternate hypothesis (if set one gets more stable bands)
    double confLevel = 0.95;                 // confidence level value

    TString minimizerType;                  // minimizer type (default is what is in ROOT::Math::MinimizerOptions::DefaultMinimizerType()
    TString massValue;                      // extra string to tag output file of result
    int printLevel = 0;                     // print level for debugging PL test statistics and calculators

    bool useNLLOffset = false;              // use NLL offset when fitting (this increase stability of fits)
    bool useNumberCounting = false;
};


class HypoTestInv {
public:
    HypoTestInv();

    explicit HypoTestInv(RooWorkspace *w) : w(w) {}

    HypoTestInv(RooWorkspace *w,
                HTIResult *r,
                TString modelSbName,
                TString modelBName,
                TString dataName,
                bool useCLs) :
            w(w), r(r),
            modelSBName(std::move(modelSbName)),
            modelBName(std::move(modelBName)),
            dataName(std::move(dataName)),
            useCLs(useCLs) {}

    virtual ~HypoTestInv();

    HTIResult *RunPoint(const TString &POI, int ntoys, int v_bin, double v_min, double v_max);

    void RecordResult(HTIResult *result, int itr, const TString& out_dir);

private:
    HypoTestInvOptions optHTInv;

    RooWorkspace *w{};
    HTIResult *r{};
    TString modelSBName = "ModelConfig";
    TString modelBName = "";
    TString dataName = "asimovData";
    bool useCLs = true;
    TString currentPOI;
};


#endif //TOYLIMIT_HYPOTESTINV_H
